\ProvidesClass{homework}[2014/01/05 version 2.0.2-SNAPSHOT]

% Doing this rigmarole of declare options, process options, and passing
% titlepage option when loading the article class, is all part of gaining the
% ability to have the 'titlepage' page option be the default, which is opposite
% that of the article class.
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions\relax
\LoadClass[titlepage]{article}

% the rule towards the bottom of the title page; only displayed on a separate
% title page
\def\titlePageRule{\rule{10cm}{0.25mm}}
% the rule separating the collaborators heading from the names of the
% collaborators on the title page; only displayed on a separate title page
\def\collaboratorsTitlePageRule{\rule{3cm}{0.1mm}}
\def\sectionRule{\rule[3mm]{\textwidth}{1pt}}
\def\subsectionRule{\rule[2mm]{\textwidth}{0.2pt}}

% subtitle for title page
\def\@subtitle{\relax}
\newif\if@subtitleSpecified
\@subtitleSpecifiedfalse
\newcommand\subtitle[1]{
  \@subtitleSpecifiedtrue
  \def\@subtitle{#1}
}
\def\outputSubtitleIfUsed{
  \if@subtitleSpecified
    \vskip 1mm
    {\Large\textbf \@subtitle} \par
  \fi
}

% author email for title page
\def\@authorEmail{\relax}
\newif\if@authorEmailSpecified
\@authorEmailSpecifiedfalse
\newcommand\authorEmail[1]{
  \def\@authorEmail{#1}
  \@authorEmailSpecifiedtrue
}
\def\outputAuthorEmailIfSpecified{
  \if@authorEmailSpecified
    {\footnotesize\texttt{<\@authorEmail>}} \par
  \fi
}

% siteinfo for title page
\def\@siteinfo{\relax}
\newif\if@siteinfoSpecified
\@siteinfoSpecifiedfalse
\newcommand\siteinfo[1]{
  \def\@siteinfo{#1}
  \@siteinfoSpecifiedtrue
}
\def\outputSiteinfoIfSpecified{
  \if@siteinfoSpecified
    \vskip 0.5mm
    {\footnotesize \@siteinfo} \par
  \fi
}


% collaborators for title page
\def\@collaborators{\relax}
\newif\if@collaboratorsSpecified
\@collaboratorsSpecifiedfalse
\newcommand\collaborators[1]{
  \def\@collaborators{#1}
  \@collaboratorsSpecifiedtrue
}

\if@titlepage
  \renewcommand\maketitle{
    \begin{titlepage}
      \let\footnotesize\small
      \let\footnoterule\relax
      \let \footnote \thanks
      \begin{center}
        {\LARGE\textbf \@title} \par
        \outputSubtitleIfUsed
        \vskip 7mm
        {\large \@author} \par
        \outputAuthorEmailIfSpecified
        \if@collaboratorsSpecified
          \vskip 10mm
          {\small In Collaboration With} \par
          \collaboratorsTitlePageRule
          \vskip 2mm
          {\small \@collaborators}
        \fi
        \vfill
        \titlePageRule \par
        \vskip 2mm
        {\small \@date} \par
        \outputSiteinfoIfSpecified
      \end{center}
      \@thanks
    \end{titlepage}%
    \setcounter{footnote}{0}%
    \global\let\thanks\relax
    \global\let\maketitle\relax
    \global\let\@thanks\@empty
    \global\let\@author\@empty
    \global\let\@date\@empty
    \global\let\@title\@empty
    \global\let\title\relax
    \global\let\author\relax
    \global\let\date\relax
    \global\let\and\relax
  }
\else
\renewcommand\maketitle{\par
  \begingroup
    \renewcommand\thefootnote{\@fnsymbol\c@footnote}%
    \def\@makefnmark{\rlap{\@textsuperscript{\normalfont\@thefnmark}}}%
    \long\def\@makefntext##1{\parindent 1em\noindent
            \hb@xt@1.8em{%
                \hss\@textsuperscript{\normalfont\@thefnmark}}##1}%
    \if@twocolumn
      \ifnum \col@number=\@ne
        \@maketitle
      \else
        \twocolumn[\@maketitle]%
      \fi
    \else
      \newpage
      \global\@topnum\z@   % Prevents figures from going at top of page.
      \@maketitle
    \fi
    \thispagestyle{plain}\@thanks
  \endgroup
  \setcounter{footnote}{0}%
  \global\let\thanks\relax
  \global\let\maketitle\relax
  \global\let\@maketitle\relax
  \global\let\@thanks\@empty
  \global\let\@author\@empty
  \global\let\@date\@empty
  \global\let\@title\@empty
  \global\let\title\relax
  \global\let\author\relax
  \global\let\date\relax
  \global\let\and\relax
}
\renewcommand\@maketitle{
  \newpage
  \null
  \vskip 2em
  \begin{center}
    \let \footnote \thanks
    {\LARGE\textbf \@title} \par
    \outputSubtitleIfUsed
    \vskip 4mm
    {\large \@author} \par
    \outputAuthorEmailIfSpecified
    \if@collaboratorsSpecified
      \vskip 4mm
      {\small In Collaboration With:} \par
      {\small \@collaborators}
    \fi
    \vskip 4mm
    {\small \@date} \par
    \outputSiteinfoIfSpecified
  \end{center}
  \par
  \vskip 1.5em}
\fi

\let\oldsection\section
\renewcommand{\section}{\@ifstar{\sectionStar}{\sectionNoStar}}
\def\sectionStar#1{\oldsection*{#1}\sectionRule}
\def\sectionNoStar#1{\oldsection{#1}\sectionRule}

\let\oldsubsection\subsection
\renewcommand{\subsection}{\@ifstar{\subsectionStar}{\subsectionNoStar}}
\def\subsectionStar#1{\oldsubsection*{#1}\subsectionRule}
\def\subsectionNoStar#1{\oldsubsection{#1}\subsectionRule}

\renewcommand\thesubsection{(\alph{subsection})}
\renewcommand\thesubsubsection{(\roman{subsubsection})}

\let\oldappendix\appendix
\renewcommand\appendix{
  \let\section\oldsection
  \let\subsection\oldsubsection
  \oldappendix
}

% Page numbering with name
\newif\ifnameForPageNumberIsSpecified
\def\nameForPageNumber#1{
  \def\@nameForPageNumber{#1}
  \nameForPageNumberIsSpecifiedtrue
}
\renewcommand{\thepage}{
  \ifnameForPageNumberIsSpecified
    \@nameForPageNumber\hspace{2mm}\arabic{page}
  \else
    \arabic{page}
  \fi
}

% Include the section number in equation numbers
\renewcommand{\theequation}{\thesection.\arabic{equation}}

\endinput
